<html>

<head>
    <title>Benutzerdetails</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" type="text/css" href="details.css"/>
</head>

<body>

<?php

require_once 'userdata.php';


$id = isset($_GET['id']) ? $_GET['id'] - 1 : 0;

?>

<h1>Benutzerdetails</h1>
<?php

//check if id is set
if (isset($_GET['id'])&& $id>=0 && $id < sizeof($data)) {

    //create table with corresponding user data
    echo
        "<table>
    <tr>
        <td>Vorname:</td>
        <td> " . $data[$id]['firstname'] . "</td>
    </tr>
    <tr>
        <td>Nachname:</td>
        <td>" . $data[$id]['lastname'] . "</td>
    </tr>
    <tr>
        <td>Geburtsdatum:</td>
        <td>" . $data[$id]['birthdate'] . "</td>
    </tr>
    <tr>
        <td>E-Mail:</td>
        <td>" . $data[$id]['email'] . "</td>
    </tr>
    <tr>
        <td>Telefon:</td>
        <td>" . $data[$id]['phone'] . "</td>
    </tr>
    <tr>
        <td>Stra&szlig;e:</td>
        <td>" . $data[$id]['street'] . "</td>
    </tr>

</table>";
} else {
    //show error message
    echo "<div class='error'>Kein Benutzer gefunden!</div>";
}

?>

<!-- go back to index.php on click  -->
<a href="../index.php">zur&uuml;ck</a>

</body>

</html>