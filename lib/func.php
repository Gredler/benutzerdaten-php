<?php

require_once "userdata.php";
$output = [];

//search for data containing user input
function search($input)
{
    global $data; //data from userdata.php
    global $output;

    foreach ($data as $value) {
        if (searchData($value['firstname'], $input)) {
            array_push($output, $value);
        } else if (searchData($value['lastname'], $input)) {
            array_push($output, $value);
        } else if (searchData($value['email'], $input)) {
            array_push($output, $value);
        }
    }

    return $output;
}

//search for user input in an array of user data (eg. array of first names)
//find substrings (strpos)
//ignore case (strtolower)
function searchData($array, $input)
{
    if (strpos(strtolower($array), strtolower($input)) !== false) {
        return true;
    }
    return false;
}

//reinitialize $output with entire user data
function clear()
{
    global $data;
    global $output;
    $output = $data;
}

//check for user input and create table with corresponding data
function selectData()
{
    global $data;
    global $output;

    //entire user data, if there is no user input
    if (!isset($_POST['input'])) {
        createTable($data);

    //only user data which matches user input
    } else {
        createTable($output);
    }

}

//create table containing user data
function createTable($userDataToDisplay)
{
    if (!empty($userDataToDisplay)) {
        foreach ($userDataToDisplay as $value) {

            $name = $value['firstname'] . ' ' . $value['lastname'];
            $email = $value['email'];
            $id = $value['id'];

            $birthdate = new DateTime($value['birthdate']);
            $birthdate = date_format($birthdate, 'd.m.Y');

            echo "<tr>
            <td><a href='lib/details.php?id=$id'>$name</a></td>
            <td><a href='lib/details.php?id=$id'>$email</a></td>
            <td><a href='lib/details.php?id=$id'>$birthdate</a></td>
            </tr>";
        }
    }else{
        echo "<tr><td colspan='3' class='error'>Keine Daten gefunden!</td></tr>";
    }
}

?>