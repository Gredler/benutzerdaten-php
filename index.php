<html>

<head>
    <title>Benutzerdaten Suchen</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" type="text/css" href="index.css"/>
</head>

<body>
<h1>Benutzerdaten suchen</h1>

<?php

require_once "lib/userdata.php";
require "lib/func.php";

$input = "";
$output = [];

//check if search button was clicked
if (isset($_POST['suchen'])) {

    //check if user entered input (=word to search for)
    $input = isset($_POST['input']) ? $_POST['input'] : "";

    //show entire data if input field is empty
    if (empty($input)) {
        clear();

    //search for user data containing user input
    } else {
        global $output;
        $output = search($input);
    }

//check if clear button was clicked
} else if (isset($_POST['leeren'])) {
    clear();
}

?>

<form method="post" action="index.php">
    <div class="form-input">
        <div class="form-element">
            <label>Suche:</label>
            <input type="text" placeholder="Suche" name="input" value="<?= htmlspecialchars($input) ?>"/>
        </div>
        <div class="form-element">
            <input type="submit" name="suchen" value="Suchen"/>
        </div>
        <div class="form-element">
            <button name="leeren">Leeren</button>
        </div>
    </div>
</form>
<div>
    <table>
        <thead>
        <tr>
            <th>Name</th>
            <th>E-Mail</th>
            <th>Geburtsdatum</th>
        </tr>
        </thead>
        <tbody>
        <?php

        //show body containing user data
        selectData();

        ?>
        </tbody>
    </table>
</div>
</body>

</html>